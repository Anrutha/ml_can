class SendPasswordResetLinkJob < ApplicationJob
  queue_as :default

  def perform(id)
    # Do something later
    @user = User.find(id)
    if Rails.env == "development"
      @user.reset_password_token = 1234 
    elsif  Rails.env == "test"
      @user.reset_password_token = 5678
    elsif Rails.env == "production"
      @user.reset_password_token = rand(2000..4000)
    end
    @user.save
    puts "##"
    puts @user.reset_password_token
    puts "##"
    MailSenderMailer.forgot_password(@user.email , @user.reset_password_token ).deliver_now
  end
end
