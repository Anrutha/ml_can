class MailSenderMailer < ApplicationMailer

  # Subject can be set in your I18n file at config/locales/en.yml
  # with the following lookup:
  #
  #   en.mail_sender_mailer.forgot_password.subject
  #
  def forgot_password(email,reset_token)
    @reset_token = reset_token
    @email = email


    mail to: @email , subject:"reset token sent"
  end
end
