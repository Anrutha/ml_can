Rails.application.routes.draw do
  
  use_doorkeeper
  devise_for :users
  # For details on the DSL available within this file, see https://guides.rubyonrails.org/routing.html

  namespace :api do
  	namespace :v1 do
      namespace :user_management do
        namespace :user do
          post 'auth/signup'
          delete 'auth/logout'
          post 'auth/login'
          post 'auth/forgot_password'
          get 'auth/view'
          post 'auth/validate_reset_password_token'
          # put 'auth/password'
        end
      end
  		resources :items, except: [:new, :edit]
    end
  end
  #resources :containers, only: :index
  
end
