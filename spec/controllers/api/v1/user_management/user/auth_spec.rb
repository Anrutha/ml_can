require 'rails_helper'

RSpec.describe "Api::V1::Auth", type: :request do

    #let!(:user) {create(:user)}

    let!(:valid_headers)         { auth_headers(user) }
    describe 'signup' do
      let!(:user){create(:user)}
      context "email already exists " do
        let!(:u1) {FactoryBot.create(:user, email: "222@gmail.com",password: "Password")}

        let(:signup_attributes){
         { user:{
            email:"222@gmail.com",
            password:"Password"
          }
        }
      }
        before {post '/api/v1/user_management/user/auth/signup', params: signup_attributes, as: :json}
        it "signup failed " do
          expect(response).to have_http_status(422)
        end
      end 
      context "email id do not exist" do
        let(:signup_attributes){
          { user:{
             email:"222@gmail.com",
             password:"Password"
           }
         }
        }
        before {post '/api/v1/user_management/user/auth/signup', params: signup_attributes, as: :json}   
        it "sign up success" do
          expect(response).to have_http_status(200)
          expect(json).not_to be_empty
          expect(json["user"]).not_to be_empty
          expect(json["token"]).not_to be_empty
        end
      end
      context "email id is blank " do
        let(:signup_attributes){
          { user:{
             password:"Password"
           }
         }
        }
        before {post '/api/v1/user_management/user/auth/signup', params: signup_attributes, as: :json}
        it " signup failed" do
          expect(response).to have_http_status(422)
        end
      end

    end

    describe 'Login' do
      let!(:user) {create(:user)}
        context "invalid password" do
          let(:valid_attributes) { 
            { 
              user:{ 
                email: "2222@gmail.com",
                password: "Password",
                grant_type: "password"
              }
            }
          }
          before { post '/api/v1/user_management/user/auth/login', params: { user => { email:"2222@gmail.com",password:"1234567"}}, as: :json}
          it "login fails" do 
            expect(response).to have_http_status(422)
          end
        end
        context '(Valid email and password)' do
    
          let(:valid_attributes) { 
            { 
              user:{ 
                email: "2222@gmail.com",
                password: "Password",
                grant_type: "password"
              }
            }
          }
    
          before { post '/api/v1/user_management/user/auth/login', params: valid_attributes, as: :json }
          #before { post '/api/v1/user_management/user/auth/login', params: { user => { email:"2222@gmail.com"}}, as: :json }

    
          it 'Token generated' do
            expect(json).not_to be_empty
            expect(json["user"]).not_to be_empty
            #raise response.body
            expect(json["token"]).not_to be_empty
            expect(response).to have_http_status(201)
          end
        end

        context '(Login by Refresh Token)' do

            let(:valid_attributes) { 
              { 
                user: { 
                  grant_type: "refresh_token",
                  refresh_token: valid_headers[:other]['refresh-token']
                }
              }
            }
      
            before { post '/api/v1/user_management/user/auth/login', params: valid_attributes, as: :json }
      
            it 'Token generated' do
              expect(json).not_to be_empty
              expect(json["user"]).not_to be_empty
              expect(json["token"]).not_to be_empty
              expect(response).to have_http_status(201)
            end
        end
        context 'email or password is blank ' do
          let(:login_attributes){
            {
              user:{
                email:"2222@gmail.com",
                grant_type: "password"
              }
            }
          }
          before { post '/api/v1/user_management/user/auth/login', params: login_attributes , as: :json}
          it " login failed" do
            expect(response).to have_http_status(422)
          end
        end
    end

    describe 'Logout' do
      let!(:user) {create(:user)}
        context '(success)' do
    
          before { delete '/api/v1/user_management/user/auth/logout', headers: valid_headers[:auth] }
    
          it 'Token Empty' do
            expect(response.body).to be_empty
            expect(response).to have_http_status(204)
          end
        end
      end

    describe 'forgot password' do
      #let!(:user) {create(:user)}
      let!(:user) {FactoryBot.create(:user, email:"abc@gmail.com")}
      context 'email id exist' do
        let(:forgot_password_attributes){
          {
            user:{
              email: "abc@gmail.com",
              redirect_url:"asdfg"
            }
          }
        }

        before { post '/api/v1/user_management/user/auth/forgot_password' , params: forgot_password_attributes, as: :json}
        it 'sends verfication link' do
          expect(response).to have_http_status(201)
          expect(json).not_to be_empty

        end
      end
      context "email id does not exist" do 
        let(:forgot_password_attributes){
         { user:{
            email:"ac@gmail.com",
            redirect_url:"asdfgh"
          }
        }
        }
        before { post '/api/v1/user_management/user/auth/forgot_password' , params: forgot_password_attributes, as: :json}
        it "do not send verification link " do
          expect(response).to have_http_status(422)
        end
    end
  end
  describe 'validate_reset_password_token' do 
    let!(:user) {FactoryBot.create(:user, email:"abc@gmail.com",reset_password_token:"12345")}
    context " reset password token is valid" do
      let(:reset_password_attributes){
        { user:{
           email:"abc@gmail.com",
           reset_password_token:12345
         }
       }
       }    
       before { post '/api/v1/user_management/user/auth/validate_reset_password_token', params: reset_password_attributes, as: :json}  
       it "sucsess" do
        expect(json).not_to be_empty
        expect(response).to have_http_status(200)
       end
    end
    context " reser password token is invalid" do 
      let(:reset_password_attributes){
        {
          user:{
            email:"abc@gmail.com",
            reset_password_token: 1234
          }
          }
        }
        before { post '/api/v1/user_management/user/auth/validate_reset_password_token' , params: reset_password_attributes , as: :json}
        it "fail" do 
          expect(response).to have_http_status(422)
        end

      
  end
  context "user not found" do
    let(:reset_password_attributes){
      {
        user:{
          email:"ab@gmail.com",
          reset_password_token: 1234
        }
        }
      } 
      before {  post '/api/v1/user_management/user/auth/validate_reset_password_token' , params: reset_password_attributes , as: :json}
      it "fails" do
        expect(response).to have_http_status(422)
      end   
  end
end


    #put 'api/v1/user_management/user/auth/reset_password?reset_password_token=adsgasdgsdfbnsdfnsdfgn&email=venkat.sai@rq.com'
    

end
