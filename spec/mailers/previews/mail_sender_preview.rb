# Preview all emails at http://localhost:3000/rails/mailers/mail_sender
class MailSenderPreview < ActionMailer::Preview

  # Preview this email at http://localhost:3000/rails/mailers/mail_sender/forgot_password
  def forgot_password
    MailSenderMailer.forgot_password
  end

end
